const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UsuariosSchema = new Schema({
    id: Number,
    name: String,
    lastname: String,
    age: Number,
    email: String,
    active: Boolean



});

module.exports = mongoose.model('usuarios', UsuariosSchema);