const express = require('express');
const router = express.Router();

const Usuario = require('../models/usuarios')

router.get('/', async (req,res)=>{
    const usuarios = await Usuario.find();
    //console.log(usuarios);
    res.render('index',{
        usuarios
    });
})

router.get('/show/:id', async (req,res)=>{
    const usuario = await Usuario.findById(req.params.id);
    //console.log(usuarios);
    res.render('show',{
        usuario
    });
})


router.post('/add', async (req,res) => {
    //console.log(new Usuario(req.body));
    const usuario = new Usuario(req.body);
    //console.log(usuario);
    try {
        await usuario.save();
    }catch(error){
        console.log(error);
    }
    
    //console.log(new Usuario(req.body));

    res.redirect('/');
})


router.get('/edit/:id', async(req,res) => {
    const id = req.params;
    //const { id } = req.params;
    //console.log(req.params.id)
    try{
        const usuario = await Usuario.findById(req.params.id);
        res.render('edit', {
            usuario
        });
    }catch(error){
        console.log('hola, no esta funcionando');
        console.log(error);
    }
    //console.log(id);

})

router.post('/edit/:id', async (req,res) => {
    //console.log(new Usuario(req.body));
    
    const id = req.params;
    //console.log(usuario); 
    try {
        
        //await Usuario.updateOne(req.params.id, req.body);
        await Usuario.updateOne({_id:req.params.id}, req.body);
        res.redirect('/');
    }catch(error){
        console.log(error);
    }
})


router.get('/delete/:id',async(req,res) => {
    const { id } = req.params;
    //console.log(req.params.id)
    await Usuario.remove({id:id});
    res.redirect('/');
})

module.exports = router;